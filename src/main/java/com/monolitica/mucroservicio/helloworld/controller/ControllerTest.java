package com.monolitica.mucroservicio.helloworld.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerTest {

    @RequestMapping("/")
    String home() {
        return "Hello CoxAuto!";
    }

}
